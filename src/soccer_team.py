import random

class SoccerTeam:

  def __init__(self, name: str , level_max: int = 5):

    self.name = name
    self.defence_level = random.randint(1, level_max - 1)
    self.attack_level = level_max - self.defence_level

  def __repr__(self):

    return "Team name: {name}\n Attack level: {attack_level}\n Defence level: {defence_level}".format(name = self.name, attack_level = self.attack_level, defence_level = self.defence_level)

  def attack(self):

    return random.randint(0, self.attack_level)
  
  def defend(self):

    return random.randint(0, self.defence_level)

  def train_attack(self):

    self.attack_level += 1
  
  def train_defence(self):
    
    self.defence_level += 1
