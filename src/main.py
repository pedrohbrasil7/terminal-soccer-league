from soccer_championship import SoccerChampionship
from soccer_team import SoccerTeam

adversary_names = ["red", "blue", "yellow", "purple", "green", "orange", "pink", "brown", "black", "grey", "white", "clouds", "sun", "moon", "sky", "grass", "wood", "water", "iron", "earth", "ufo", "aliens", "fire", "air"]

def run_game():
  # Gets the number of players and their teams names
  n_players = int(input('Welcome to My Soccer Championship Game! Please enter the number of players.\n'))
  players_team_names = []
  for player_n in range(n_players):
    players_team_names.append(input("Player {player_n}, please enter your team's name.\n".format(player_n = player_n + 1)))
  # Sets the championship's name and year
  championship_name = "My Soccer League"
  championship_year = 2022
  # Creates the players' teams
  championship_teams = []
  for i in range(len(players_team_names)):
    championship_teams.append(SoccerTeam(players_team_names[i]))
  # Creates the adversary teams (bots)
  n_adversary_teams = int(input('How many other adversary teams would you like to have in your championship?\n'))
  for i in range(n_adversary_teams):
    team_name_index = int(i / len(adversary_names))
    championship_teams.append(SoccerTeam(adversary_names[i] + "_" + str(team_name_index)))
  # Runs the game
  championship = SoccerChampionship(championship_name, championship_teams, championship_year)
  championship.run_championship(players_team_names)

# if __name__ == "__main__": 
#     run_game()
run_game()