import random
import string
from soccer_team import SoccerTeam

class SoccerChampionship:

  def __init__(self, name: str, teams: list, year: int):

    self.name = name
    self.teams = teams
    self.year = year
    self.matches = []
    self.scores = []
    self.championship_points = {}

    for team in teams:
      self.championship_points[team.name] = 0
      
    self.generate_matches()

  def __repr__(self):
    
    description = "{name} - {year}\n".format(name = self.name, year = self.year)
    i = 0

    for team_name,points in sorted(self.championship_points.items(), key = lambda team_points:(team_points[1], team_points[0]), reverse = True):
      i += 1
      description += str(i) + " - " + str(points) + " - " + team_name + "\n"
    
    return description

  def generate_matches(self):

    all_matches = [] 
    for i_match in range(len(self.teams)):
      team1 = self.teams[i_match]

      for i_round in range(len(self.teams)):
        team2 = self.teams[i_round]

        if team1.name != team2.name:
          all_matches.append((team1, team2))

    i_round = 0
    while len(all_matches) > 0:

      self.matches.append([all_matches.pop(0)])
      self.scores.append([(-1, -1)])
      i_match = 0

      while i_match < len(all_matches):

        should_append = True

        for i_current_matches in range(len(self.matches[i_round])):

          if (self.matches[i_round][i_current_matches][0] == all_matches[i_match][0] or \
          self.matches[i_round][i_current_matches][1] == all_matches[i_match][0] or \
          self.matches[i_round][i_current_matches][0] == all_matches[i_match][1] or \
          self.matches[i_round][i_current_matches][1] == all_matches[i_match][1]):
            should_append = False
            break

        if should_append:
          self.matches[i_round].append(all_matches.pop(i_match))
          self.scores[i_round].append((-1, -1))
        else:
          i_match += 1

      i_round += 1

  def run_goal_attempt(self, attack_team: SoccerTeam, defence_team: SoccerTeam):

    is_goal = False
    attack_team_score = attack_team.attack()
    defence_team_score = defence_team.defend()

    if attack_team_score > defence_team_score:
      kick_accuracy = random.random()
      keeper_defence = random.random()

      if (kick_accuracy > 0.8 or (kick_accuracy > 0.7 and keeper_defence < 0.5)):
          is_goal = True

    return is_goal

  def run_match(self, team1: SoccerTeam, team2: SoccerTeam):

    score_team1 = 0
    score_team2 = 0

    for minute in range(90):

      if minute % 2 == 0:
        is_goal = self.run_goal_attempt(team1, team2)
        if is_goal: score_team1 += 1
      else:
        is_goal = self.run_goal_attempt(team2, team1)
        if is_goal: score_team2 += 1

    return (score_team1, score_team2)

  def train_team(self, team_name: str):

    for i in range(len(self.teams)):

      if self.teams[i].name == team_name:
        train_attack_or_defence = random.random()
        
        if train_attack_or_defence > 0.5: self.teams[i].train_attack()
        else: self.teams[i].train_defence()

  def train_user_team(self, team_name: str):

    for i in range(len(self.teams)):

      if self.teams[i].name == team_name:
        train_attack_or_defence = " "

        while train_attack_or_defence != "a" and train_attack_or_defence != "d":
          train_attack_or_defence = input("Would you like to improve the attack \"a\" or the defence \"d\"? The current levels are:\n - Attack: {attack_level}\n - Defence: {defence_level}\n".format(attack_level = self.teams[i].attack_level, defence_level = self.teams[i].defence_level))

          if train_attack_or_defence.lower() == "a":
            self.teams[i].train_attack()
          elif train_attack_or_defence.lower() == "d":
            self.teams[i].train_defence()
          else:
            print("Invalid entry. Please try again.")

  def run_championship(self, players_team_names: list):

    print("\nLet the games begin!\n")
    for i_round in range(len(self.matches)):
      for i_match in range(len(self.matches[i_round])):
        team1 = self.matches[i_round][i_match][0]
        team2 = self.matches[i_round][i_match][1]

        # Checks if the teams are player's teams
        i_team1_player = -1
        i_team2_player = -1
        if team1.name in players_team_names: i_team1_player = players_team_names.index(team1.name)
        if team2.name in players_team_names: i_team2_player = players_team_names.index(team2.name)

        # Interacts with player
        if team1.name in players_team_names or team2.name in players_team_names:

          if i_team1_player != -1 and i_team2_player != -1:
            print("Players {team1_player_n} and {team2_player_n}, it's time for your next match. It's {team1} x {team2}!\n".format(team1_player_n = i_team1_player + 1, team2_player_n = i_team2_player + 1,team1 = team1.name, team2 = team2.name))
          elif i_team1_player != -1:
            print("Player {team1_player_n}, it's time for your next match. It's {team1} x {team2}!\n".format(team1_player_n = i_team1_player + 1,team1 = team1.name, team2 = team2.name))
          elif i_team2_player != -1:
            print("Player {team2_player_n}, it's time for your next match. It's {team1} x {team2}!\n".format(team2_player_n = i_team2_player + 1,team1 = team1.name, team2 = team2.name))
            
          print("Here are some stats:\n")
          print(team1)
          print(team2)
          input('Press "Enter" to run the match!\n')

        # Runs the match
        scores = self.run_match(team1, team2)
        self.scores[i_round][i_match] = scores
        if team1.name in players_team_names or team2.name in players_team_names:
          print("The match is finished. Here's the final score:\n {team1} {score_team1} - {score_team2} {team2}\n".format(team1 = team1.name, team2 = team2.name, score_team1 = self.scores[i_round][i_match][0], score_team2 = self.scores[i_round][i_match][1]))

        # Updates championship points
        if self.scores[i_round][i_match][0] > self.scores[i_round][i_match][1]:
          self.championship_points[team1.name] += 3
          if team1.name in players_team_names or team2.name in players_team_names:
            print("{team1} wins!\n".format(team1 = team1.name))
        elif self.scores[i_round][i_match][0] == self.scores[i_round][i_match][1]:
          self.championship_points[team1.name] += 1
          self.championship_points[team2.name] += 1
          if team1.name in players_team_names or team2.name in players_team_names:
            print("It's a tie!\n")
        elif self.scores[i_round][i_match][0] < self.scores[i_round][i_match][1]:
          self.championship_points[team2.name] += 3
          if team1.name in players_team_names or team2.name in players_team_names:
            print("{team2} wins!\n".format(team2 = team2.name))

        # Trains the teams
        if team1.name in players_team_names and team2.name in players_team_names:
          print("Player {team1_player_n}, it's time to train your team {team1_name}!".format(team1_player_n = i_team1_player + 1, team1_name = team1.name))
          self.train_user_team(team1.name)
          print("Player {team2_player_n}, it's time to train your team {team2_name}!".format(team2_player_n = i_team1_player + 1, team2_name = team2.name))
          self.train_user_team(team2.name)
        elif team1.name in players_team_names:
          print("Player {team1_player_n}, it's time to train your team {team1_name}!".format(team1_player_n = i_team1_player + 1, team1_name = team1.name))
          self.train_user_team(team1.name)
          self.train_team(team2.name)
        elif team2.name in players_team_names:
          self.train_team(team1.name)
          print("Player {team2_player_n}, it's time to train your team {team2_name}!".format(team2_player_n = i_team1_player + 1, team2_name = team2.name))
          self.train_user_team(team2.name)
        else:
          self.train_team(team1.name)
          self.train_team(team2.name)
      
      print("Here's the results of round {round}/{n_rounds} matches:\n".format(round = i_round + 1, n_rounds = len(self.matches)))
      for i_match in range(len(self.matches[i_round])):
        print(str(self.matches[i_round][i_match][0].name) + " " + str(self.scores[i_round][i_match][0]) + " - " + str(self.scores[i_round][i_match][1]) + " " + str(self.matches[i_round][i_match][1].name))
      input('Press "Enter" to continue.')
      print("\nHere's what the championship points table looks like:\n")
      print(self)
      input('Press "Enter" to continue.')

    print("Alright! The championship is over! Thanks for playing!")
